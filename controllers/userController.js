const User = require('../models/users')
const bcrypt = require('bcrypt')
const auth = require('../auth')

// Check if email exists
module.exports.checkIfEmailExists = (data) => {
	if (data.isAdmin) {
		return User.find({ email: data.email }).then((result) => {
			if (result.length > 0) {
				return true
			}
			return {
				message: 'User does not exist.'
			}
		})
	}
	let message = Promise.resolve({ message: "Non-admins cannot use this function!" })
	return message.then((value) => {
		return value
	})
}


// Register new user
module.exports.register = (data) => {
	let encrypted_password = bcrypt.hashSync(data.password, 10)

	let new_user = new User({
		email: data.email,
		password: encrypted_password
	})

	return new_user.save().then((created_user, error) => {
		if (error) {
			return false
		}

		return {
			message: 'User successfully registered!'
		}
	})
}

// Login user
module.exports.login = (data) => {
	return User.findOne({ email: data.email }).then((result) => {
		if (result == null) {
			return {
				message: "User doesn't exist!"
			}
		}

		const is_password_correct = bcrypt.compareSync(data.password, result.password)

		if (is_password_correct) {
			return {
				accessToken: auth.createAccessToken(result)
			}
		}

		return {
			message: 'Password is incorrect!'
		}
	})
}

// Get user details
module.exports.getUserDetails = (user_id) => {
	return User.findById(user_id, { password: 0 }).then((result) => {
		return result
	})
}

// Update a user (Admin only)
module.exports.updateUser = (user_Id, data) => {
	if (data.isAdmin) {
		return User.findByIdAndUpdate(user_Id).then((result, error) => {
			if (error) {
				return false
			}
			result.isAdmin = data.isAdmin
			return result.save().then((updatedUser, error) => {
				if (error) {
					return false
				}
				return {
					message: 'User was set to admin'
				}
			})
		})
	}
	let message = Promise.resolve({ message: "Non-admins cannot use this function!" })
	return message.then((value) => {
		return value
	})
}

// Get all users
module.exports.getAllUsers = (data) => {
	if (data.isAdmin) {
		return User.find({}, { password: 0 }).then((result) => {
			return result
		})
	}
	let message = Promise.resolve({ message: "Non-admins cannot use this function!" })
	return message.then((value) => {
		return value
	})
}


