const Order = require('../models/orders')

// Check cart 
module.exports.addOrder = (data) => {
    // let cart = [
    //     userId: data.order.userId,
    //     productId: data.order.productId,
    //     quantity: data.order.quantity,
    // ]
    const new_order = new Order({
        userId: data.order.userId,
        productId: data.order.productId,
        quantity: data.order.quantity,
    })

    return new_order.save().then((new_order, error) => {
        if (error) {
            return false
        }
        return { message: "New Order Successfully Created!" }
    })
}

// Retrieve all orders(Admin only)
module.exports.getAllOrders = (data) => {
    if (data.isAdmin)
        return Order.find({}).then((result) => {
            return result
        })
    let message = Promise.resolve({ message: "Non admins cannot use this function!" })
    return message.then((value) => {
        return value
    })
}



