const Product = require('../models/products')

// Add product (Admin only)
module.exports.addProduct = (data) => {
	if (data.isAdmin) {
		let new_product = new Product({
			name: data.product.name,
			description: data.product.description,
			price: data.product.price
		})
		return new_product.save().then((new_product, error) => {
			if (error) {
				return false
			}
			return {
				message: 'Product has been successfully added!'
			}
		})
	}
	let message = Promise.resolve({
		message: 'Non-admins cannot use this function!'
	})
	return message.then((value) => {
		return value
	})
}

// Update product (Admin only)
module.exports.updateProduct = (data) => {
	if (data.isAdmin) {
		return Product.findByIdAndUpdate(data.id, {
			name: data.product.name,
			description: data.product.description,
			price: data.product.price,
			isActive: data.product.isActive
		}).then((updated_product) => {
			if (updated_product !== null) {
				return {
					message: "Product updated successfully",
					product: updated_product
				}
			}
			return {
				message: "Oops you encountered a problem!"
			}
		}).catch((error) => {
			console.log(error);
			return {
				message: "Invalid product ID!"
			}
		})
	}
	return Promise.resolve({
		message: "Non-admins cannot use this function!"
	})
}

// Archive product (Admin only)
module.exports.archiveProduct = (product_id, isAdmin) => {
	if (isAdmin) {
		return Product.findByIdAndUpdate(product_id, {
			isActive: false
		}).then((updated_product) => {
			if (updated_product !== null) {
				return {
					message: "Product archived successfully."
				}
			}

			return {
				message: "Unable to find product."
			}
		}).catch((error) => {
			console.log(error)
			return {
				message: "Invalid product Id."
			}
		})
	}

	return Promise.resolve({
		message: "Non-admins cannot use this function!"
	})
}

// Get all products
module.exports.getAllProducts = () => {
	return Product.find({}).then((result) => {
		return result
	})
}

// Get all active products
module.exports.getAllActive = () => {
	return Product.find({ isActive: true }).then((result) => {
		return result
	})
}

// Get a specific product by ID
module.exports.getProduct = (product_id) => {
	return Product.findById(product_id).then((result) => {
		return result
	})
}
