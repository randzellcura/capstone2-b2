const express = require('express')
const router = express.Router()
const userController = require('../controllers/userController')
const auth = require('../auth')

// Check if email exists
router.post("/check-email", auth.verify, (request, response) => {
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	userController.checkIfEmailExists(request.body, data).then((result) => {
		response.send(result)
	})
})

// Register new user
router.post("/register", auth.verify, (request, response) => {
	userController.register(request.body).then((result) => {
		response.send(result)
	})
})

// Get user details
router.get("/:id/details", auth.verify, (request, response) => {
	userController.getUserDetails(request.params.id).then((result) => {
		response.send(result)
	})
})

// Login user
router.post("/login", (request, response) => {
	userController.login(request.body).then((result) => {
		response.send(result)
	})
})

// Update a user
router.patch('/update/:userId', auth.verify, (request, response) => {
	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}

	userController.updateUser(request.params.userId, data).then((result) => {
		response.send(result)
	})
})

// Get all users 
router.get('/', auth.verify, (request, response) => {
	const data = {
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	userController.getAllUsers(data).then((result) => {
		response.send(result)
	})
})


module.exports = router