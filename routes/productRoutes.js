const express = require('express')
const router = express.Router()
const productController = require('../controllers/productController')
const auth = require('../auth')

// Admin functions

// Add product (Admin only)
router.post('/addProduct', auth.verify, (request, response) => {
	const data = {
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	productController.addProduct(data).then((result) => {
		response.send(result)
	})
})

// Update product (Admin only)
router.patch('/update/:id', auth.verify, (request, response) => {
	const data = {
		id: request.params.id,
		product: request.body,
		isAdmin: auth.decode(request.headers.authorization).isAdmin
	}
	productController.updateProduct(data).then((result)=> {
		response.send(result)
	})
})

// Archive product (Admin only)
router.patch('/archive/:id', auth.verify, (request,response) => {
	const isAdmin = auth.decode(request.headers.authorization).isAdmin
	productController.archiveProduct(request.params.id, isAdmin).then((result) => {
		response.send(result)
	})
})


// Normal User functions

// Get all products
router.get('/', (request, response) => {
	productController.getAllProducts().then((result) => {
		response.send(result)
	})
})

// Get all active products
router.get('/active', (request, response) => {
	productController.getAllActive().then((result) => {
		response.send(result)
	})
})

// Get a specific product by ID
router.get('/:productId', (request, response) => {
	productController.getProduct(request.params.productId).then((result) => {
		response.send(result)
	})
})

module.exports = router